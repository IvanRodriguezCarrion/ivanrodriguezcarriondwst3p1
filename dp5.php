<!DOCTYPE html>

<html >
<head>
  <meta charset="utf-8" />
  <title>dp5</title>
  <link href="estilo.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<h1>DATOS PERSONALES 5 (RESULTADO)</h1>
<?php
function valores($cine, $literatura, $musica) {
    // usando la funcion si deuelve null o no podemos averiguar si 
    //clicado o no, no es necesario evaluar que devuelve.
    $gustaCin = isset($_REQUEST[$cine]);
    $gustaLit = isset($_REQUEST[$literatura]);
    $gustaMus = isset($_REQUEST[$musica]);
    
    if ($gustaCin && $gustaLit && $gustaMus) {
	echo ("<p>Te gusta <strong>todo</strong>.</p>\n");
    } elseif (!$gustaCin && !$gustaLit && !$gustaMus) {
	echo ("<p>No te gusta nada <strong>nada</strong>.</p>\n");
    } 
    
    if ($gustaCin && !$gustaLit && !$gustaMus) {
	echo ("<p>Te gusta el <strong>cine</strong>.</p>\n");
    } elseif ($gustaCin && $gustaLit && !$gustaMus) {
	echo ("<p>Te gusta el <strong>cine</strong> y la <strong>literatura</strong>.</p>\n");
    } elseif ($gustaCin && !$gustaLit && $gustaMus) {
	echo ("<p>Te gusta el <strong>cine</strong> y la <strong>musica</strong>.</p>\n");
    }
    
    if ($gustaCin && !$gustaLit && !$gustaMus) {
	echo ("<p>Te gusta la <strong>literatura</strong>.</p>\n");
    } elseif (!$gustaCin && $gustaLit && $gustaMus) {
	echo ("<p>Te gusta la <strong>literatura</strong> y la <strong>musica</strong>.</p>\n");
    } 
    
    if (!$gustaCin && !$gustaLit && $gustaMus) {
	    echo ("<p>Te gusta la <strong>música</strong>.</p>\n");
    }
    
    echo ("<p><a href=\"dp5.html\">Volver al formulario.</a></p>\n");
}

valores("cine", "literatura", "musica");

?>

</body>
</html>
