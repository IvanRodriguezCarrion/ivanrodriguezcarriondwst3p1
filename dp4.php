<!DOCTYPE html>

<html >
<head>
  <meta charset="utf-8" />
  <title>dp4</title>
  <link href="estilo.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<h1>DATOS PERSONALES 4 (RESULTADO)</h1>
<?php
function datos($valor)
{
    if (isset($_REQUEST[$valor])){ // isset control null
	    if ($_REQUEST[$valor] == "hombre") {
		echo ("<p>Eres un <strong>hombre</strong>.</p>\n");
		echo ("<p><a href=\"dp4.html\">Volver al formulario.</a></p>\n");
	    } elseif ($_REQUEST[$valor] == "mujer") {
		echo ("<p>Eres una <strong>mujer</strong>.</p>\n");
		echo ("<p><a href=\"dp4.html\">Volver al formulario.</a></p>\n");
	    }   
	} else {
	    echo ("<p class=\"aviso\">No eres <strong>nada</strong>. Escoge una opción.</p>\n");
	    echo ("<p><a href=\"dp4.html\">Volver al formulario.</a></p>\n");
	}
}

datos("rse");
?>

</body>
</html>
