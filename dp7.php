<!DOCTYPE html>

<html >
<head>
  <meta charset="utf-8" />
  <title>dp7</title>
  <link href="estilo.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<h1>DATOS PERSONALES 6 (RESULTADO)</h1>
<?php
function pagina($nombre, $apellidos, $edad, $peso, $genero, $estadoCivil, $cine, $literatura, $musica) {  
    if (isset($_REQUEST[$nombre])){ // isset control null
	    $resultado =strip_tags(trim($_REQUEST[$nombre]));
    } 	
    if ($_REQUEST[$nombre]==""){
	   echo ("<p class=\"aviso\">Debe escribir un nombre en la caja de texto</p>\n");
    } else {
	    echo("<p>Tu nombre es <strong>$resultado</strong>.</p>");
    }
    
    
    if (isset($_REQUEST[$apellidos])){ // isset control null
	    $resultado =strip_tags(trim($_REQUEST[$apellidos]));
    }
    if ($_REQUEST[$apellidos]==""){
	   echo ("<p class=\"aviso\">Debe escribir un apellido al menos en la caja de texto</p>\n");
    } else {
	    echo("<p>Tus apellidos son <strong>$resultado</strong>.</p>");
    }
    
    if (isset($_REQUEST[$edad])){ // isset control null
	    if ($_REQUEST[$edad] == "1") {
		echo ("<p>Tienes <strong>menos de 20</strong> años.</p>\n");
	    } elseif ($_REQUEST[$edad] == "2") {
		echo ("<p>Tienes <strong>entre 20 y 39</strong> años.</p>\n");
	    } elseif ($_REQUEST[$edad] == "3") {
		echo ("<p>Tienes <strong>entre 40 y 60</strong> años.</p>\n");
	    } elseif ($_REQUEST[$edad] == "4") {
		echo ("<p>Tienes <strong>más de 60</strong> años.</p>\n");
	    } else {
		echo ("<p>No tienes edad <strong>alguna</strong>. Escoge una opción.</p>\n");
	    }
    }
    
    if (isset($_REQUEST[$peso])){ // isset control null
	    $resultado =strip_tags(trim($_REQUEST[$peso]));
    }
    if ($_REQUEST[$peso]==""){
	   echo ("<p class=\"aviso\">Debe escribir un apellido al menos en la caja de texto</p>\n");
    } else {
	    echo("<p>Tus apellidos son <strong>$resultado</strong>.</p>");
    }
    
    if (isset($_REQUEST[$genero])){ // isset control null
	    if ($_REQUEST[$genero] == "hombre") {
		echo ("<p>Eres un <strong>hombre</strong>.</p>\n");
	    } elseif ($_REQUEST[$genero] == "mujer") {
		echo ("<p>Eres una <strong>mujer</strong>.</p>\n");
	    }   
	} else {
	    echo ("<p>No tienes <strong>género</strong>. Escoge una opción.</p>\n");
	}
	
    if (isset($_REQUEST[$estadoCivil])){ // isset control null
       if ($_REQUEST[$estadoCivil] == "casado") {
	   echo ("<p>Estás <strong>casad@</strong>.</p>\n");
       } elseif ($_REQUEST[$estadoCivil] == "soltero") {
	   echo ("<p>Estás <strong>solter@</strong>.</p>\n");
       } else {
	   echo ("<p>Eres <strong>otra cosa</strong>.</p>\n");
       }  
   } else {
       echo ("<p>No existes en el <strong>estado civil</strong>. Escoge una opción.</p>\n");
   }
   
   $gustaCin = isset($_REQUEST[$cine]);
    $gustaLit = isset($_REQUEST[$literatura]);
    $gustaMus = isset($_REQUEST[$musica]);
    
    if ($gustaCin && $gustaLit && $gustaMus) {
	echo ("<p>Te gusta <strong>todo</strong>.</p>\n");
    } elseif (!$gustaCin && !$gustaLit && !$gustaMus) {
	echo ("<p>No te gusta nada <strong>nada</strong>.</p>\n");
    } 
    
    if ($gustaCin && !$gustaLit && !$gustaMus) {
	echo ("<p>Te gusta el <strong>cine</strong>.</p>\n");
    } elseif ($gustaCin && $gustaLit && !$gustaMus) {
	echo ("<p>Te gusta el <strong>cine</strong> y la <strong>literatura</strong>.</p>\n");
    } elseif ($gustaCin && !$gustaLit && $gustaMus) {
	echo ("<p>Te gusta el <strong>cine</strong> y la <strong>musica</strong>.</p>\n");
    }
    
    if ($gustaCin && !$gustaLit && !$gustaMus) {
	echo ("<p>Te gusta la <strong>literatura</strong>.</p>\n");
    } elseif (!$gustaCin && $gustaLit && $gustaMus) {
	echo ("<p>Te gusta la <strong>literatura</strong> y la <strong>musica</strong>.</p>\n");
    } 
    
    if (!$gustaCin && !$gustaLit && $gustaMus) {
	echo ("<p>Te gusta la <strong>música</strong>.</p>\n");
    }
        
   echo ("<p><a href=\"dp7.html\">Volver al formulario.</a></p>\n"); 
}

pagina("nombre", "apellidos", "edad", "peso", "genero", "estadoCivil", "cine", "literatura", "musica");
?>

</body>
</html>
