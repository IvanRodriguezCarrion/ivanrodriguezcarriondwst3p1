<!DOCTYPE html>

<html >
<head>
  <meta charset="utf-8" />
  <title>dp6</title>
  <link href="estilo.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<h1>DATOS PERSONALES 6 (RESULTADO)</h1>
<?php
function cBox($valor) {  
    if (isset($_REQUEST[$valor])){ // isset control null
	    if ($_REQUEST[$valor] == "1") {
		echo ("<p>Tienes <strong>menos de 20</strong> años.</p>\n");
		echo ("<p><a href=\"dp6.html\">Volver al formulario.</a></p>\n");
	    } elseif ($_REQUEST[$valor] == "2") {
		echo ("<p>Tienes <strong>entre 20 y 39</strong> años.</p>\n");
		echo ("<p><a href=\"dp6.html\">Volver al formulario.</a></p>\n");
	    } elseif ($_REQUEST[$valor] == "3") {
		echo ("<p>Tienes <strong>entre 40 y 60</strong> años.</p>\n");
		echo ("<p><a href=\"dp6.html\">Volver al formulario.</a></p>\n");
	    } elseif ($_REQUEST[$valor] == "4") {
		echo ("<p>Tienes <strong>más de 60</strong> años.</p>\n");
		echo ("<p><a href=\"dp6.html\">Volver al formulario.</a></p>\n");
	    } else {
		echo ("<p>No tienes edad <strong>alguna</strong>. Escoge una opción.</p>\n");
		echo ("<p><a href=\"dp6.html\">Volver al formulario.</a></p>\n");
	    }
    } 
}

cBox("edad");
?>

</body>
</html>
